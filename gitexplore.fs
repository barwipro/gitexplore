open gitload
[<EntryPoint>]
let main argv =
    printfn "%A" argv
    analyse argv.[0]
    0 // return an integer exit code
