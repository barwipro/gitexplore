module gitload 

    
    open Deedle
    open FSharp.Data
    open XPlot.GoogleCharts
    open XPlot.GoogleCharts.Deedle
    open LibGit2Sharp
    open FSharp.Collections.ParallelSeq

    let getCommiters (commits: IQueryableCommitLog) = 
        commits
        |> Seq.map ( fun commit -> commit.Author.Name)
        |> Set.ofSeq

    let rec exploreTree (repo: Repository) (treeItem: TreeEntry) =
        match treeItem.TargetType with
        | TreeEntryTargetType.Tree -> 
            repo.Lookup<Tree> (treeItem.Target.Sha)// repo.Lookup<Tree>(treeItem.Target.sha)
            |> Seq.map (fun x -> exploreTree repo x)
            |> Seq.concat
        | _ -> Seq.ofArray([|treeItem|])


    let getFiles (repo: Repository) =
        repo.Commits
        |> Seq.map (fun commit -> commit.Tree)
        |> Seq.concat
        |> Seq.map (fun x -> exploreTree repo x)
        |> Seq.concat
        |> Seq.map(fun x -> x.Path)
        |> Set.ofSeq


    let mutable some = 0

    let getFileName (name: string) =
        name.Substring(name.LastIndexOf("/") + 1)

    let getChanges (repo: Repository) (commit: Commit) =
        if Seq.isEmpty commit.Parents = false 
        then
            let commitParent = Seq.head commit.Parents
            let change = repo.Diff.Compare<TreeChanges>(commitParent.Tree, commit.Tree);
            some <- some + 1
            printfn "changin %A" some
            change.Modified
            |> Seq.map (fun s -> getFileName s.Path)
            |> Seq.filter (fun path -> path.Contains(".fs") || path.Contains(".java"))
            |> Seq.toArray
        else
            [||]

    let getModifiedFiles (repo: Repository) =
        repo.Commits
        |> Seq.map (fun commit -> getChanges repo commit)

    let analyse pathToRepo =
        let repo = new Repository(pathToRepo)

        printfn "startin"
        let commiters = getCommiters repo.Commits
        printfn "got commiters"
        //let files = getFiles repo
        printfn "got files"
        let modifiedInCommit = getModifiedFiles repo
        //printfn "got modified %A %A" (Seq.head modifiedInCommit) (Seq.length modifiedInCommit)
        let temporal = 
            modifiedInCommit
            |> Seq.collect (fun files ->
                if Seq.isEmpty files = false 
                then
                    let filesArr = files
                    filesArr
                    |> Array.mapi (fun idx l ->  
                        if idx = (filesArr.Length) - 1
                        then [||]
                        else filesArr.[idx+1..]
                            |> Array.map(fun r -> l, r))
                    |> Seq.collect id
                else    
                    Seq.empty)
            |> Seq.groupBy id
            |> Seq.map (fun ((file1, file2), gr) -> 
                file1, file2, Seq.length gr)
            |> Seq.sortByDescending (fun (_, _, count) -> count)

        let s = 
            temporal
            |> Seq.toArray

        let s2 = 
            s
            |> Seq.map(fun (f1, f2, gr) -> (getFileName(f1), getFileName(f2), gr))

        let chart = Chart.Sankey s2
        System.IO.File.WriteAllText("test.html", chart.Html); 
        printfn "%A" s2


